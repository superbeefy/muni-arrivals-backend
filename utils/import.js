var fs = require('fs'),
    mysql = require('mysql'),
    connection = mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: 'roflcake',
        database: 'muni'
    }),
    route_directions = [],
    trip_ids = [];

connection.connect(function(err){
    if(err) throw err;
});

var parseBase = function(err, data) {
    this.table = null;

    if(err) throw err;
};


var parseRouteData = function(err, data){
    if(err) throw err;
    console.log("Processing route data...");

    // Split into lines
    var lines = data.split('\r\n');
    lines.shift();
    lines.pop();

    connection.query('TRUNCATE routes');
    for(var i = 0; i < lines.length; i++) {
        var row = lines[i].split(','),
            cols = [
                row[0] | 0,
                '"' + row[2].trim() + '"',
                '"' + row[3].trim() + '"',
                row[5] = row[5] | 0,
                0
            ];


        if(cols[3] === 3){
            if(row[2].match(/[0-9]{1,2}L$/)) {
                cols[4] = 1;
            } else if(row[2].match(/[0-9]{1,2}([A,B])?X$/)) {
                cols[4] = 2;
            } else if(row[2].match(/OWL$/)) {
                cols[4] = 3;
            }
        }

        var query = 'INSERT INTO routes (id,abbr,name,route_type_id,route_subtype_id) VALUES(' + cols.join(',') + ')';
        connection.query(query);
    }
};

var parseStopData =function(err, data){
    if(err) throw err;
    console.log("Processing stop data...");

    var lines = data.split('\r\n');
    lines.shift();
    lines.pop();

    connection.query('TRUNCATE stops');

    for(var i = 0; i < lines.length; i++) {
        var rows = lines[i].split(','),
            cols = [
                rows[0] | 0,
                '"' + rows[1].trim() + '"',
                rows[3],
                rows[4]
            ],
            query = 'INSERT INTO stops (id,name,latitude,longitude) VALUES(' + cols.join(',') + ')';
        connection.query(query);
    }

    connection.end();
};

var parseStopTimeData = function(err, data) {
    if(err) throw err;

    console.log("Processing stop time data...");

    var lines = data.split('\r\n');
    lines.shift();
    lines.pop();

    connection.query('TRUNCATE stop_sequence');

    for(var i = 0; i < lines.length; i++) {
        // search for record
        var rows = lines[i].split(','),
            cols = [
                rows[0] | 0,
                rows[3] | 0,
                rows[4] | 0
            ],
            query = 'INSERT INTO stop_sequence (trip_id, stop_id, stop_sequence) VALUES(' + cols.join(',') + ')';

        if(trip_ids.indexOf(rows[0]) > -1) {
            connection.query(query);
        }
    }

    connection.end();
};

var parseTripData = function(err,data) {
    if(err) throw err;
    console.log("Processing trip data...");

    var lines = data.split('\r\n');
    lines.shift();
    lines.pop();

    connection.query('TRUNCATE route_directions');

    for(var i = 0; i < lines.length; i++) {
        var rows = lines[i].split(','),
            cols = [
                rows[0] | 0,
                rows[4] | 0,
                rows[2] | 0,
                '"' + rows[3]  + '"'
            ],
            query = 'INSERT IGNORE INTO route_directions (route_id, direction, trip_id, headsign) VALUES(' + cols.join(',') + ')';

        if(route_directions.indexOf(rows[0] + '-' + rows[4]) === -1) {
            route_directions.push(rows[0] + '-' + rows[4]);
            trip_ids.push(rows[2]);
        }
        connection.query(query);
    }
};

// read file in
fs.readFile('../data/routes.txt', {encoding: 'ascii'}, parseRouteData);
fs.readFile('../data/stops.txt', {encoding: 'ascii'}, parseStopData);
fs.readFile('../data/trips.txt', {encoding: 'ascii'}, parseTripData);
fs.readFile('../data/stop_times.txt', {encoding: 'ascii'}, parseStopTimeData);
