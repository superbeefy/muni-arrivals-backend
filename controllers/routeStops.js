var RouteModel = require('../models/routes');

exports.init = function(app, connection) {
  var path = /^\/route\/(([JKLMNT]-OWL)|([0-9]{1,2}([LABX]{1,2})?)|(NX)|[a-zA-Z]+(-[a-zA-Z]+)?)(\/(outbound|inbound))?$/;

  RouteModel.init(connection);

  app.get(path, function(req,res){
    var direction = req.params.pop(),
        route = req.params[0],
        stops = RouteModel.fetchStopList(route, direction);

    stops.then(function(results){
      res.send(JSON.stringify(results));
    }, function(err){
      res.send('Server Error', 500);
      console.error('Database Error');
      console.error(err);
    });
  });
};


