var StopModel = require('../models/stops'),
    nextmuni  = require('../lib/nextmuni');

exports.init = function(app, connection) {
  var path = /^\/nearest\/(-?\d+(.\d+)?)\/(-?\d+(.\d+)?)$/;

  StopModel.init(connection);

  app.get(path, function(req, res){
    var latitude = req.params.shift();
    req.params.shift();
    var longitude = req.params.shift(),
        stops = StopModel.fetchNearest(latitude, longitude);

    stops.then(function(results){
      return nextmuni.multiPrediction(results);
    }, function(err){
      res.send('Server Error', 500);
      console.error('Database Error');
      console.error(err);
    }).then(function(results){
      res.send(JSON.stringify(results));
    }, function(err){
      res.send('Server Error', 500);
      console.log('Database Error');
      console.log(err);
    });
  });
};
