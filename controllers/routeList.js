var RouteModel = require('../models/routes.js');

exports.init = function(app, connection) {
  var path = '/routes';

  RouteModel.init(connection);

  app.get(path, function(req,res){
    var routes = RouteModel.fetchRoutes();
    routes.then(function(results){
      res.send(JSON.stringify(results));
    }, function(err){
      res.send('Server Error', 500);
      console.error('Database error');
      console.error(err);
    });
  });
};
