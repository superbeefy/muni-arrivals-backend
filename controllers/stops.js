var StopModel = require('../models/stops.js'),
    nextmuni  = require('../lib/nextmuni.js');

exports.init = function(app, connection) {
  var path = /^\/stop\/(([JKLMNT]-OWL)|([0-9]{1,2}([LABX]{1,2})?)|(NX)|[a-zA-Z]+(-[a-zA-Z]+)?)\/(\d+)?$/;

  StopModel.init(connection);

  app.get(path, function(req,res){
    var reqId = req.params.pop(),
        route = req.params[0],
        stop = StopModel.fetchByStopId(reqId);

    stop.then(function(results){
      var muni = nextmuni.prediction(results[0].id, route);
      return muni;
    }, function(err){
      res.send('Server Error', 500);
      console.error("Database error");
      console.error(err);
    }).then(function(results){
      res.send(JSON.stringify(results[0]));
    }, function(err){
      res.send('Server Error', 500);
      console.error('Next Muni error');
      console.error(err);
    });
  });
};

