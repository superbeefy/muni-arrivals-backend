#MUNI Arrivals Backend

This is the backend portion of the Muni Arrivals project, part of the
Uber coding challenge.

Technical track: fullstack.

For this part of the project I wanted to choose something that was going to 
be lightweight, and allowed me to get started quickly. I ended up settling on 
node.js using express. The reasoning behind the choice to seprate the projects 
out is to also help dividing duties, if this were a production app. The backend 
consists of the application as well as a small MySQL database that holds a local 
copy of the muni route and stops list.The reasoning behind this decision was two 
fold first I wanted to reduce API calls to the nextmuni service, not only because 
it adds delay fufiling to a request, but also I didn't want to spam it either. The 
nextmuni service is only used to retrive prediction data for stops. The second 
reason to maintain a database was to allow geolocation based queries.

Again due to time constraints I have skipped a lot of testing. However, I do believe this 
structure will allow easier testing of the business logic components. There are a few tests 
that cover the about 1/3 of the business logic. This is one of my first projects using 
express. If I had more time I would have completed all unit testing, added analytics via 
statsd, and logstash. In addition I would have wrote some deployment scripts to make the 
process easier.


##API
* base url: {domain}
* routes
* route/{routeId}/{direction}
* nearest/{latitude}/{longitude}
* stop/{routeId}/{stopId}

##Known bugs
If the current line doesn't have a prediction the app doesn't show any indication of failure.

##Deploying
* clone this repo
* Setup MySQL server. Import sql dump for the database located in the fixtures folder. The file
  is called live.sql
* Install node
* Install grunt server with command sudo npm install -g grunt-server
* fetch submodule using git submodule update --remote
* enter submodule directory and do a npm install, then run grunt to generate a dist version of files
* back out to root dir and run npm install for express
* modify config file to set the production database passwords
* node app.js to start server

##Development
There are a few things that a different from deploying. Once the repo is cloned vagrant up, and perform
the rest of the config inside the vagrant VM. If there was more time this would have been a precompiled
base box with everything needed. Also when you start up the server be sure the set the env so the
correct configuration can be set.

NODE_ENV="dev" node app.js

##Unit Testing
* sudo npm install -g jasmine-node
* run tests in root dir with this command NODE_ENV="unit" jasmine-node spec
