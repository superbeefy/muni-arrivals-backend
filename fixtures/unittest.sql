-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 27, 2014 at 04:15 AM
-- Server version: 5.5.35
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `muni_ut`
--

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL,
  `abbr` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `route_type_id` tinyint(4) NOT NULL,
  `route_subtype_id` tinyint(4) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `abbr`, `name`, `route_type_id`, `route_subtype_id`) VALUES
(1000, '1', 'LRV Route 1', 0, 0),
(1001, '2', 'Bus Route 2', 3, 0),
(1003, '3L', 'Bus Route 3 Limited', 3, 1),
(1004, '4X', 'Bus Route 4 Express', 3, 2),
(1005, '5-Owl', 'Bus Route 5 Owl', 3, 3),
(1006, '6', 'Cable Car 6', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `route_directions`
--

CREATE TABLE IF NOT EXISTS `route_directions` (
  `route_id` int(11) NOT NULL,
  `direction` tinyint(4) NOT NULL,
  `trip_id` int(11) NOT NULL,
  `headsign` varchar(64) NOT NULL,
  UNIQUE KEY `route_id` (`route_id`,`direction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route_directions`
--

INSERT INTO `route_directions` (`route_id`, `direction`, `trip_id`, `headsign`) VALUES
(1001, 0, 1, 'Outbound'),
(1001, 1, 2, 'Inbound');

-- --------------------------------------------------------

--
-- Table structure for table `stops`
--

CREATE TABLE IF NOT EXISTS `stops` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stops`
--

INSERT INTO `stops` (`id`, `name`, `latitude`, `longitude`) VALUES
(500, 'Stop 1', 0, 0),
(501, 'Stop 2', 0, 0),
(502, 'Stop 3', 0, 0),
(503, 'Stop 4', 0, 0),
(504, 'Stop 5', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `stop_sequence`
--

CREATE TABLE IF NOT EXISTS `stop_sequence` (
  `trip_id` int(11) NOT NULL,
  `stop_id` int(11) NOT NULL,
  `stop_sequence` int(11) NOT NULL,
  KEY `trip_id` (`trip_id`,`stop_id`,`stop_sequence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stop_sequence`
--

INSERT INTO `stop_sequence` (`trip_id`, `stop_id`, `stop_sequence`) VALUES
(2, 500, 10),
(2, 501, 11),
(2, 502, 12),
(2, 503, 13),
(2, 504, 14);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
