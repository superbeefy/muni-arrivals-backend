var connection = null,
    Promise = require('promise');

exports.init = function(conn) {
  connection = conn;
};

exports.fetchByStopId = function(stopId) {
  return new Promise(function(resolve, reject){
    var query = "SELECT * FROM stops WHERE id = ? LIMIT 1";
    connection.query(query, [stopId], function(err,results){
        if(err) reject(err);
        resolve(results);
    });
  });
};

exports.fetchNearest = function(latitude, longitude) {
  return new Promise(function(resolve, reject){
    var units = {
          feet: {
            u: 365228,
            r: 500
          },
          miles: {
            u: 69,
            r: 1
          },
          km: {
            u: 111.045,
            r: 0.5
          }
        },
        unit = units.feet.u,
        radius = units.feet.r;

    // Query within radius using haversine formula http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/
    var query = [
      'SELECT s.id, s.name, s.latitude, s.longitude, s.distance, rt.abbr, rd.direction',
      'FROM (',
        'SELECT id, name, latitude, longitude,r,',
            unit + '* DEGREES(ACOS(COS(RADIANS(latpoint))',
                   '* COS(RADIANS(latitude))',
                   '* COS(RADIANS(longpoint) - RADIANS(longitude))',
                   '+ SIN(RADIANS(latpoint))',
                   '* SIN(RADIANS(latitude)))) AS distance',
        'FROM stops',
        'JOIN (',
          'SELECT  ' + latitude + ' AS latpoint, ' + longitude + ' AS longpoint, ' + radius + ' AS r',
        ') AS p',
        'WHERE latitude',
          'BETWEEN latpoint  - (r / ' + unit + ')',
            'AND latpoint  + (r / ' + unit + ')',
        'AND longitude',
          'BETWEEN longpoint - (r / (' + unit + ' * COS(RADIANS(latpoint))))',
            'AND longpoint + (r / (' + unit + ' * COS(RADIANS(latpoint))))',
      ') s',
      'INNER JOIN stop_sequence AS ss ON s.id = ss.stop_id',
      'INNER JOIN route_directions AS rd ON ss.trip_id = rd.trip_id',
      'INNER JOIN routes AS rt ON rd.route_id = rt.id',
      'WHERE distance <= r AND rt.route_subtype_id != 3',
  //        'GROUP BY s.id',
      'ORDER BY distance'
    ].join(' ');

    connection.query(query, function(err, results){
      if(err) reject(err);

      var stops = [];
      for(var i = 0; i < results.length; i++) {
        stops.push({
          route: results[i].abbr,
          stop_id: results[i].id
        });
      }

      resolve(stops);
    });
  });
};
