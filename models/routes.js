var connection = null,
    Promise = require('promise');

exports.init = function(conn) {
  connection = conn;
};

exports.fetchRoutes = function() {
  return new Promise(function(resolve, reject){
    var query = [
      'SELECT *, 0 AS isExpress, 0 AS isLimited, 0 AS isOwl FROM routes WHERE route_type_id = 0',
      'UNION ALL',
      'SELECT *, 0 AS isExpress, 0 AS isLimited, 0 AS isOwl FROM routes WHERE route_type_id = 5',
      'UNION ALL',
      'SELECT *, IF(route_subtype_id = 2, 1, 0) AS isExpress,',
        'IF(route_subtype_id = 1, 1, 0) AS isLimited,',
        'IF(route_subtype_id = 3, 1, 0) AS isOwl',
        'FROM routes WHERE route_type_id = 3'
    ].join(' ');
    connection.query(query, function(err,results){
      if(err) reject(err);
      resolve(results);
    });
  });
};

exports.fetchStopList = function(routeId, direction) {
  return new Promise(function(resolve, reject){
    var query = '';

    if( direction === 'inbound' || direction == 'outbound') {
      direction = (direction === 'inbound') | 0;
      query = "SELECT s.id, s.name, '" + routeId + "' AS route " +
          "FROM routes AS r " +
          "INNER JOIN route_directions AS rd ON r.id = rd.route_id AND rd.direction = ? " +
          "INNER JOIN stop_sequence AS ss ON rd.trip_id = ss.trip_id " +
          "INNER JOIN stops AS s ON ss.stop_id = s.id " +
          "WHERE r.abbr LIKE ? " +
          "ORDER BY ss.stop_sequence ASC ";
      connection.query(query,[direction,routeId], function(err,results){
        if(err) reject(err);
        resolve(results);
      });
    }
  });
};

