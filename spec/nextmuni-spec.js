var nextmuni = require('../lib/nextmuni');

describe('single stop prediction', function(){
  it('should fetch a list of predictions', function(done){
    var muni = nextmuni.prediction(6619, 'L');
    muni.then(function(result){
      expect(result.length).toEqual(1);
      expect(result[0].number).toEqual('L');
      done();
    }, function(err){
      expect(err).toBeNull();
      done();
    });
  });

  it('should fail when no predictions exists', function(done){
    var muni = nextmuni.prediction(6619, 'L-OWL');
    muni.then(function(result){
    }, function(err){
      expect(err).not.toBeNull();
      done();
    });
  });
});

describe('multi stop prediction', function(){
  it('should fetch a list of predictions', function(done){
    var muni = nextmuni.multiPrediction([
      {route: 'L', stop_id: 6619},
      {route: 'N', stop_id: 6992},
      {route: 'L', stop_id: 6992}
    ]);

    muni.then(function(result){
      expect(result.length).toEqual(3);
      expect(result[0].number).toEqual('L');
      expect(result[1].number).toEqual('N');
      expect(result[2].number).toEqual('L');
      done();
    }, function(err){
      expect(err).toBeNull();
      done();
    });
  });

  it('should fail when no prediction exists for any line', function(done){
    var muni = nextmuni.multiPrediction([
      {route: 'L', stop_id: 6619},
      {route: 'N', stop_id: 6992},
      {route: 'L-OWL', stop_id: 6992}
    ]);

    muni.then(function(result){
      expect(result).toBeNull();
      done();
    }, function(err){
      expect(err).not.toBeNull();
      done();
    });
  });
});
