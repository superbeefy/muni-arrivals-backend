var RouteModel = require('../models/routes'),
    mysql      = require('mysql'),
    config     = require('../config/config');

describe('Fetch Routes', function(){
  var connection;

  beforeEach(function(){
    connection = mysql.createConnection(config.database);

    RouteModel.init(connection);
  });

  afterEach(function(){
    connection.end();
  });


  it('Should return a list of routes', function(done){
    var routes = RouteModel.fetchRoutes();

    routes.then(function(results){
      expect(results.length).toEqual(6);
      expect(results[0].id).toEqual(1000);
      expect(results[1].id).toEqual(1006);
      done();
    }, function(err){
      expect(err).toBeNull();
      done();
    });
  });
});

describe('Fetch stop list', function(){
  var connection;

  beforeEach(function(){
    connection = mysql.createConnection(config.database);

    RouteModel.init(connection);
  });

  afterEach(function(){
    connection.end();
  });

  it('Should return a list of stops from a route', function(done){
    var routes = RouteModel.fetchStopList(2, 'inbound');

    routes.then(function(results){
      expect(results.length).toEqual(5);
      done();
    }, function(err){
      expect(err).toBeNull();
      done();
    });

  });


  it('Should return a empty list if no route exist', function(done){
    var routes = RouteModel.fetchStopList('bad', 'inbound');

    routes.then(function(results){
      expect(results.length).toEqual(0);
      done();
    }, function(err){
      expect(err).toBeNull();
      done();
    });

  });
});
