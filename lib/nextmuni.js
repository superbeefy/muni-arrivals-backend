var http = require('http'),
    Promise = require('promise'),
    parser = require('xml2js').parseString,
    host = 'webservices.nextbus.com',
    baseEndpoint = '/service/publicXMLFeed?a=sf-muni',
    options = {
      hostname: host,
      port: 80,
      method: 'GET'
    };

var handleRequest = function(res, resolve, reject) {
  var xml = "";
  res.on('data', function(chunk){
    xml += chunk;
  }).on('end', function(){
    var tmp = xml.split('\n');
    tmp.shift();
    xml = tmp.join('\n');
    parser(xml, function(err, result){
      if(err) reject(err);

      //resolve(result);
      //return;

      // Parse results to normalized format before we resolve the promise
      if(typeof result.body['Error'] !== 'undefined' || typeof result.body.predictions === 'undefined') {
        if(result.body['Error']) {
          reject(result.body['Error']);
        }
        reject('no predictions');
        return;
      }
      var parsed = [];
      for(var i = 0, predictions = result.body.predictions; i < predictions.length; i++) {
        var stop = {};

        stop.route        = predictions[i].$.routeTitle;
        stop.number       = predictions[i].$.routeTag;
        stop.name         = predictions[i].$.stopTitle;
        stop.directions   = [];


        for(var j = 0, directions = predictions[i].direction; j < directions.length; j++) {
          var direction = {};

          direction.name        = directions[j].$.title;
          direction.predictions = [];

          for(var k = 0, d_predictions = directions[j].prediction; k < d_predictions.length; k++) {
            var prediction = {};

            prediction.minutes = d_predictions[k].$.minutes;

            if(prediction.minutes <= 1) {
              prediction.seconds = d_predictions[k].$.seconds;
            }

            direction.predictions.push(prediction);
          }

          stop.directions.push(direction);
        }

        parsed.push(stop);
      }

      resolve(parsed);
    });
  });
};

/**
 * Handles predictions for a single stop
 */
exports.prediction = function(stopId, route) {
  return new Promise(function(resolve,reject){
    options.path = baseEndpoint + '&command=predictions&s=' + stopId + '&r=' + route;

    var request = http.request(options, function(res){
      handleRequest(res, resolve, reject);
    });

    request.on('error', function(err){
      reject(err);
    });
    request.end();
  });
};

/**
 * Handles predictions for multiple stops
 */
exports.multiPrediction = function(stops) {
  return new Promise(function(resolve,reject){
    var url = baseEndpoint;
    for(var i = 0; i < stops.length; i++) {
        url += "&command=predictionsForMultiStops&stops=" + stops[i].route + "|" + stops[i].stop_id;
    }

    options.path = url;
    var request = http.request(options, function(res){
      handleRequest(res, resolve, reject);
    });

    request.on('error', function(err){
      reject(err);
    });

    request.end();
  });
};
