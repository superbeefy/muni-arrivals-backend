var config = {};

config.application = {
  port: 80
}

config.database = {
  // Enter Production database creds here
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'muni'
};

switch(process.env.NODE_ENV) {
  case 'dev':
  case 'development':
    config.application.port: 3000;
    config.database.password = 'roflcake';
    break;
  case 'ut':
  case 'unit':
    config.database.password = 'roflcake';
    config.database.database = 'muni_ut';
    break;
}

module.exports = config;
