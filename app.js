var express = require('express'),
    mysql = require('mysql'),
    config = require('./config/config'),
    app = express(),
    connection = mysql.createConnection(config.database);

app.configure(function(){
    app.use(function(req,res,next){
        res.setHeader("Access-Control-Allow-Origin", "*");
        return next();
    });
});

app.use(express.static(__dirname + '/public'));

app.get('/', function(req,res){
    res.sendFile('public/index.html');
});

// Load up controllers
var controllers = [
  'nearest',
  'routeList',
  'routeStops',
  'stops'
].map(function(controller){
  var controller = require('./controllers/' + controller);
  controller.init(app, connection);
});

console.log("Server started listening on port " + config.application.port);
app.listen(config.application.port);
